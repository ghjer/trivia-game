# Trivia

## Demo
Play the game [here](https://trivia-game-anton.herokuapp.com/).

## Wireframe
See the wireframe [here](trivia-wireframe.pdf).

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Deploy
To deploy to heroku:
```
git push heroku master
```

To see the latest logs:
```
heroku logs --tail
```

View your app:
```
heroku open
```

## Inspiration
For the color system I was inspired by [Material Design](https://material.io/design/color/the-color-system.html#color-usage-and-palettes).