export const TriviaAPI = {
    getQuestions(details) {
        return fetch(`https://opentdb.com/api.php?${details}`)
            .then(response => response.json())

        // Use dummy data to save the API endpoint during development
        // console.log(details);
        // return {response_code: 0, results: [{"category":"General Knowledge","type":"multiple","difficulty":"medium","question":"In a standard set of playing cards, which is the only king without a moustache?","correct_answer":"Hearts","incorrect_answers":["Spades","Diamonds","Clubs"]},{"category":"General Knowledge","type":"multiple","difficulty":"easy","question":"In the video-game franchise Kingdom Hearts, the main protagonist, carries a weapon with what shape?","correct_answer":"Key","incorrect_answers":["Sword","Pen","Cellphone"]},{"category":"General Knowledge","type":"multiple","difficulty":"hard","question":"What is the airspeed velocity of an unladen swallow?","correct_answer":"24 MPH","incorrect_answers":["15 MPH","20 MPH","200 MPH"]},{"category":"General Knowledge","type":"multiple","difficulty":"medium","question":"Scotch whisky and Drambuie make up which cocktail?","correct_answer":"Rusty Nail","incorrect_answers":["Screwdriver","Sex on the Beach","Manhattan"]},{"category":"General Knowledge","type":"multiple","difficulty":"medium","question":"Which of these companies does NOT manufacture automobiles?","correct_answer":"Ducati","incorrect_answers":["Nissan","GMC","Fiat"]},{"category":"General Knowledge","type":"multiple","difficulty":"medium","question":"What is the unit of currency in Laos?","correct_answer":"Kip","incorrect_answers":["Ruble","Konra","Dollar"]},{"category":"General Knowledge","type":"boolean","difficulty":"easy","question":"A scientific study on peanuts in bars found traces of over 100 unique specimens of urine.","correct_answer":"False","incorrect_answers":["True"]},{"category":"General Knowledge","type":"multiple","difficulty":"medium","question":"What is the Italian word for &quot;tomato&quot;?","correct_answer":"Pomodoro","incorrect_answers":["Aglio","Cipolla","Peperoncino"]},{"category":"General Knowledge","type":"multiple","difficulty":"medium","question":"Which of these words means &quot;idle spectator&quot;?","correct_answer":"Gongoozler","incorrect_answers":["Gossypiboma","Jentacular","Meupareunia"]},{"category":"General Knowledge","type":"multiple","difficulty":"medium","question":"The Mexican Beer &quot;Corona&quot; is what type of beer?","correct_answer":"Pale Lager","incorrect_answers":["India Pale Ale","Pilfsner","Baltic Porter"]}]}
    },
    getCategories() {
        return fetch('https://opentdb.com/api_category.php')
            .then(response => response.json())
            .then(data => data.trivia_categories)

        // Use dummy data to save the API endpoint during development
        // return [{"id":9,"name":"General Knowledge"},{"id":10,"name":"Entertainment: Books"},{"id":11,"name":"Entertainment: Film"},{"id":12,"name":"Entertainment: Music"},{"id":13,"name":"Entertainment: Musicals & Theatres"},{"id":14,"name":"Entertainment: Television"},{"id":15,"name":"Entertainment: Video Games"},{"id":16,"name":"Entertainment: Board Games"},{"id":17,"name":"Science & Nature"},{"id":18,"name":"Science: Computers"},{"id":19,"name":"Science: Mathematics"},{"id":20,"name":"Mythology"},{"id":21,"name":"Sports"},{"id":22,"name":"Geography"},{"id":23,"name":"History"},{"id":24,"name":"Politics"},{"id":25,"name":"Art"},{"id":26,"name":"Celebrities"},{"id":27,"name":"Animals"},{"id":28,"name":"Vehicles"},{"id":29,"name":"Entertainment: Comics"},{"id":30,"name":"Science: Gadgets"},{"id":31,"name":"Entertainment: Japanese Anime & Manga"},{"id":32,"name":"Entertainment: Cartoon & Animations"}]
    },
    getToken() {
        return fetch('https://opentdb.com/api_token.php?command=request')
            .then(response => response.json())

        // Use dummy data to save the API endpoint during development
        // return {response_code: 0, token: 'dummy_token'}
    }
}