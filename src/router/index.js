import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "home" */ '../components/Home/Home.vue')
    },
    {
        path: '/trivia',
        name: 'Trivia',
        component: () => import(/* webpackChunkName: "trivia" */ '../components/Trivia/Questions.vue')
    },
    {
        path: '/results',
        name: 'Results',
        component: () => import(/* webpackChunkName: "result" */ '../components/Result/Results.vue')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router