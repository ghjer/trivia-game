import Vue from 'vue'
import Vuex from 'vuex'
import { TriviaAPI} from '@/components/Trivia/TriviaAPI'
import router from '../router'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        numberOfQuestions: 10,
        difficulty: 'easy',
        selectedCategory: '10',
        categories: ['Loading...'],
        questionIndex: 0,
        questions: [],
        error: '',
        token: '',
        isLoading: true
    },
    mutations: {
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setAnswer: (state, payload) => {
            state.questions[state.questionIndex].answer = payload
        },
        setQuestionIndex: (state, payload) => {
            state.questionIndex = payload
        },
        setNumberOfQuestions: (state, payload) => {
            state.numberOfQuestions = payload
        }, 
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        }, 
        setSelectedCategory: (state, payload) => {
            state.selectedCategory = payload
        }, 
        setCategories: (state, payload) => {
            state.categories = payload
        }, 
        setError: (state, payload) => {
            state.error = payload
        },
        setToken: (state, payload) => {
            state.token = payload
        },
        setLoading: (state, payload) => {
            state.isLoading = payload
        }
    },
    getters: {
        answers: state => {
            const answers = [...state.questions[state.questionIndex].incorrect_answers, state.questions[state.questionIndex].correct_answer]
            const shuffle = (a) => {
                for (let i = a.length - 1; i > 0; i--) {
                    const j = Math.floor(Math.random() * (i + 1));
                    [a[i], a[j]] = [a[j], a[i]];
                }
                return a;
            }
            return shuffle(answers)
        }
    //     score: state => {
    //         return state.questions.reduce((score, q) => {
    //             return q.answer === q.correct_answer ? score + 10 : score + 1
    //         }, 0)
    //     }
    },
    actions: {
        async getTriviaToken({ commit }) {
            try {
                const response = await TriviaAPI.getToken()

                if(response.response_code === 0) {
                    commit('setToken', response.token)
                } else {
                    commit('setError', `Error getting questions ${response.response_code}`)
                }
            } catch (e) {
                commit('setError', e.message)
            }
        },
        async getTriviaQuestions({ state, commit }) {
            commit('setLoading', true)
            commit('setQuestionIndex', 0)
            commit('setError', '')

            if(state.token === '') {
                this.dispatch('getTriviaToken');
            }

            try {
                let questionDetails = `amount=${state.numberOfQuestions}`
                questionDetails += `&category=${state.selectedCategory}`
                questionDetails += `&difficulty=${state.difficulty}`
                questionDetails += `&token=${state.token}`
                const response = await TriviaAPI.getQuestions(questionDetails)

                if(response.response_code === 0) {
                    commit('setQuestions', response.results)
                } else if(response.response_code === 3) {
                    commit('setToken', '')
                    this.dispatch('getTriviaQuestions');
                } else {
                    commit('setError', `Error getting questions ${response.response_code}`)
                }
            } catch (e) {
                commit('setError', e.message)
                commit('setLoading', false)
            }
            commit('setLoading', false)
        },
        async getTriviaCategories({ commit }) {
            commit('setLoading', true)
            try {
                const categories = await TriviaAPI.getCategories()

                if(categories) {
                    commit('setCategories', categories)
                } else {
                    commit('setError', 'Error getting categories')
                }
            } catch (e) {
                commit('setError', e.message)
            }

            commit('setLoading', false)
        },
        goToNextQuestion({ state, commit }) {
            if(state.numberOfQuestions - 1 > state.questionIndex) {
                commit('setQuestionIndex', state.questionIndex + 1)
            } else {
                commit('setQuestionIndex', 0)
                router.push('/results')
            }
        },
        answerQuestion({ commit }, answer) {
            commit('setAnswer', answer)
        }
    }
})